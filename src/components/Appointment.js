import React from "react";
import PropTypes from "prop-types";

import "../css/appointment.css";
import Button from "./controls/Button";

export default class Appointment extends React.Component {
    render() {
        return (
            <Button
                name={`${this.props.name} (£${this.props.price})`}
                smallFont={true}
                onClick={this.props.onClick}
            />
        );
    }
}

Appointment.propTypes = {
    id: PropTypes.number,
    name: PropTypes.string,
    category: PropTypes.string,
    price: PropTypes.string,
    onClick: PropTypes.func,
};
