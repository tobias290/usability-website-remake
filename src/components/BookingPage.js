import React from "react";
import "../css/booking-page.css";
import BookAppointment from "./BookAppointment";

export default class BookingPage extends React.Component {
    render() {
        return (
            <div id="booking-page">
               <BookAppointment />
            </div>
        );
    }
}
