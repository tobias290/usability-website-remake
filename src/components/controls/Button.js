import React from "react";
import PropTypes from "prop-types";

import "../../css/controls/button.css";

export default class Button extends React.Component {
    render() {
        return (
            <div className={`button ${this.props.smallFont ? "button--small-font" : ""}`} onClick={this.props.onClick}>
                {this.props.name}
                <i className="fas fa-chevron-right button__arrow" />
            </div>
        );
    }
}

Button.propTypes = {
    name: PropTypes.string,
    smallFont: PropTypes.bool,
    onClick: PropTypes.func,
};

Button.defaultProps = {
    smallFont: false
};
