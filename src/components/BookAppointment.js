import React from "react";
import PropTypes from "prop-types";
import "../css/book-appointment.css";

import dummyData from "../dummy_data";

import Button from "./controls/Button";
import Appointment from "./Appointment";

export default class BookAppointment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            formData: sessionStorage.hasOwnProperty("formData") ? JSON.parse(sessionStorage.getItem("formData")) : {
                step: 1,
                isExistingPatient: null,
                category: null,
                location: null,
                practitioner: null,
                timeSlot: null,
                date: null,
                payment: {
                    firstName: null,
                    lastName: null,
                    dob: {
                        day: null,
                        month: null,
                        year: null,
                    },
                    mobileNumber: null,
                    cardDetails: {
                        number: null,
                        cvc: null,
                        expiry: {
                            month: null,
                            year: null,
                        }
                    }
                }
            }
        };

        this.setStep = this.setStep.bind(this);
        this.stepIncrease = this.stepIncrease.bind(this);
        this.stepDecrease = this.stepDecrease.bind(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        sessionStorage.setItem("formData", JSON.stringify(this.state.formData));
    }

    getTitle() {
        switch (this.state.formData.step) {
            case 1: return "Patient Type";
            case 2: return "Appointment Type";
            case 3: return "Pick Appointment";
            case 4: return "Pick Location";
            case 5: return "Pick Practitioner";
            case 6: return "Pick Time Slot";
            case 7: return "Pick Date";
            case 8: return "Payment";
            case 9: return "Confirmation";
            default: return "";
        }
    }

    setStep(step) {
        let newState = this.state;
        newState.formData.step = step;
        this.setState(newState);
    }

    stepIncrease() {
        let newState = this.state;
        newState.formData.step += 1;
        this.setState(newState);
    }

    stepDecrease() {
        let newState = this.state;
        newState.formData.step -= 1;
        this.setState(newState);
    }

    render() {
        return (
            <div>
                <div className="bread-crumbs">
                    {this.state.formData.step < 4 &&
                    <>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 1 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(1)}
                        >Patient Type
                        </button>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 2 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(2)}
                        >Appointment Type
                        </button>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 3 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(3)}
                        >Pick Appointment
                        </button>
                        <button
                            className="bread-crumbs__crumb"
                            onClick={() => this.setStep(4)}
                        >Next
                        </button>
                    </>
                    }

                    {this.state.formData.step >= 4 && this.state.formData.step < 7 &&
                    <>
                        <button
                            className="bread-crumbs__crumb"
                            onClick={() => this.setStep(3)}
                        >Previous
                        </button>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 4 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(4)}
                        >Pick Location
                        </button>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 5 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(5)}
                        >Pick Practitioner
                        </button>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 6 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(6)}
                        >Pick Time Slot
                        </button>
                        <button
                            className="bread-crumbs__crumb"
                            onClick={() => this.setStep(7)}
                        >Next
                        </button>
                    </>
                    }

                    {this.state.formData.step >= 7 &&
                    <>
                        <button
                            className="bread-crumbs__crumb"
                            onClick={() => this.setStep(6)}
                        >Previous
                        </button>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 7 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(7)}
                        >Pick Date
                        </button>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 8 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(8)}
                        >Payment
                        </button>
                        <button
                            className={`bread-crumbs__crumb ${this.state.formData.step === 9 ? "bread-crumbs--active" : ""}`}
                            onClick={() => this.setStep(9)}
                        >Confirmation
                        </button>
                    </>
                    }
                </div>
                <div id="book-appointment">
                    <div id="mobile-title">
                        {this.getTitle()}
                    </div>

                    {this.state.formData.step === 1 &&
                    <>
                        <Button name={"New Patient"} onClick={() => {
                            this.stepIncrease();
                            this.setState({formData: {...this.state.formData, isExistingPatient: false}});
                        }}/>
                        <Button name={"Existing Patient"} onClick={() => {
                            this.stepIncrease();
                            this.setState({formData: {...this.state.formData, isExistingPatient: true}});
                        }}/>
                    </>
                    }

                    {this.state.formData.step === 2 &&
                    <>
                        <div className="button button--back" onClick={this.stepDecrease}>
                            <i className="fas fa-chevron-left button__arrow"/>
                            &nbsp;&nbsp;&nbsp;
                            Back
                        </div>
                        {dummyData.categories.map((category, i) =>
                            <Button
                                key={i}
                                name={category}
                                onClick={() => {
                                    this.stepIncrease();
                                    this.setState({
                                        formData: {
                                            ...this.state.formData,
                                            category: category
                                        }
                                    });
                                }}
                            />
                        )}
                    </>
                    }

                    {this.state.formData.step === 3 &&
                    <>
                        <div className="button button--back" onClick={this.stepDecrease}>
                            <i className="fas fa-chevron-left button__arrow"/>
                            &nbsp;&nbsp;&nbsp;
                            Back
                        </div>

                        {dummyData.appointments.filter(appointment => appointment.category === this.state.formData.category || this.state.formData.category === "Everything").map((appointment, i) =>
                            <Appointment key={i} {...appointment} onClick={this.stepIncrease}/>
                        )}
                    </>
                    }

                    {this.state.formData.step === 4 &&
                    <>
                        <div className="button button--back" onClick={this.stepDecrease}>
                            <i className="fas fa-chevron-left button__arrow"/>
                            &nbsp;&nbsp;&nbsp;
                            Back
                        </div>

                        {dummyData.locations.map((location, i) =>
                            <Button
                                key={i}
                                name={location}
                                onClick={() => {
                                    this.stepIncrease();
                                    this.setState({
                                        formData: {
                                            ...this.state.formData,
                                            location: location
                                        }
                                    });
                                }}
                            />
                        )}
                    </>
                    }

                    {this.state.formData.step === 5 &&
                    <>
                        <div className="button button--back" onClick={this.stepDecrease}>
                            <i className="fas fa-chevron-left button__arrow"/>
                            &nbsp;&nbsp;&nbsp;
                            Back
                        </div>

                        {dummyData.practitioner.map((practitioner, i) =>
                            <Button
                                key={i}
                                name={practitioner}
                                onClick={() => {
                                    this.stepIncrease();
                                    this.setState({
                                        formData: {
                                            ...this.state.formData,
                                            practitioner: practitioner
                                        }
                                    });
                                }}
                            />
                        )}
                    </>
                    }

                    {this.state.formData.step === 6 &&
                    <>
                        <div className="button button--back" onClick={this.stepDecrease}>
                            <i className="fas fa-chevron-left button__arrow"/>
                            &nbsp;&nbsp;&nbsp;
                            Back
                        </div>

                        {dummyData.time_slots.map((timeSlot, i) =>
                            <Button
                                key={i}
                                name={timeSlot}
                                onClick={() => {
                                    this.stepIncrease();
                                    this.setState({
                                        formData: {
                                            ...this.state.formData,
                                            timeSlot: timeSlot
                                        }
                                    });
                                }}
                            />
                        )}
                    </>
                    }

                    {this.state.formData.step === 7 &&
                        <form className="form">
                            {/*<div className="button button--back" onClick={this.stepDecrease}>*/}
                            {/*    <i className="fas fa-chevron-left button__arrow"/>*/}
                            {/*    &nbsp;&nbsp;&nbsp;*/}
                            {/*    Back*/}
                            {/*</div>*/}
                            <div className="form__input-container">
                                <label className="form__label">Pick Date*</label>
                                <input
                                    className="form__input"
                                    type="date"
                                    onChange={(e) => {
                                        this.stepIncrease();
                                        this.setState({formData: {...this.state.formData, date: e.target.value}});
                                    }}
                                />
                            </div>
                            <div className="form__options-container">
                                <input className="form__submit" type="button" value="Back" onClick={this.stepDecrease} />
                            </div>
                        </form>
                    }

                    {this.state.formData.step === 8 &&
                    <>
                        <form className="form" onSubmit={this.stepIncrease}>
                            <div className="form__input-container">
                                <label className="form__label">First Name *</label>
                                <input
                                    className="form__input"
                                    type="text"
                                    required
                                    placeholder="Please enter your first name"
                                    onChange={(e) => {
                                        this.setState({
                                            ...this.state,
                                            formData: {
                                                ...this.state.formData,
                                                payment: {
                                                    ...this.state.formData.payment,
                                                    firstName: e.target.value,
                                                }
                                            }
                                        });
                                    }}
                                />
                            </div>
                            <div className="form__input-container">
                                <label className="form__label">Last Name *</label>
                                <input
                                    className="form__input"
                                    type="text"
                                    required
                                    placeholder="Please enter your last name"
                                    onChange={(e) => {
                                        this.setState({
                                            ...this.state,
                                            formData: {
                                                ...this.state.formData,
                                                payment: {
                                                    ...this.state.formData.payment,
                                                    lastName: e.target.value,
                                                }
                                            }
                                        });
                                    }}
                                />
                            </div>
                            <div className="form__input-container">
                                <label className="form__label">Date of Birth *</label>
                                <select className="form__input form__input--small" required placeholder="Day" onChange={(e) => {
                                    this.setState({
                                        ...this.state,
                                        formData: {
                                            ...this.state.formData,
                                            payment: {
                                                ...this.state.formData.payment,
                                                dob: {
                                                    ...this.state.formData.payment.dob,
                                                    day: e.target.value,
                                                }
                                            }
                                        }
                                    });
                                }}>
                                    <option selected disabled>Day</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="21">31</option>
                                </select >
                                <select className="form__input form__input--small" required placeholder="Month" onChange={(e) => {
                                    this.setState({
                                        ...this.state,
                                        formData: {
                                            ...this.state.formData,
                                            payment: {
                                                ...this.state.formData.payment,
                                                dob: {
                                                    ...this.state.formData.payment.dob,
                                                    month: e.target.value,
                                                }
                                            }
                                        }
                                    });
                                }}>
                                    <option selected disabled>Month</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select className="form__input form__input--small" required placeholder="Year" onChange={(e) => {
                                    this.setState({
                                        ...this.state,
                                        formData: {
                                            ...this.state.formData,
                                            payment: {
                                                ...this.state.formData.payment,
                                                dob: {
                                                    ...this.state.formData.payment.dob,
                                                    year: e.target.value,
                                                }
                                            }
                                        }
                                    });
                                }}>
                                    <option selected disabled>Year</option>
                                    <option value="2020">2020</option>
                                    <option value="2019">2019</option>
                                    <option value="2018">2018</option>
                                    <option value="2017">2017</option>
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2014</option>
                                    <option value="2013">2013</option>
                                    <option value="2012">2012</option>
                                    <option value="2011">2011</option>
                                    <option value="2010">2010</option>
                                    <option value="2009">2009</option>
                                    <option value="2008">2008</option>
                                    <option value="2007">2007</option>
                                    <option value="2006">2006</option>
                                    <option value="2005">2005</option>
                                    <option value="2004">2004</option>
                                    <option value="2003">2003</option>
                                    <option value="2002">2002</option>
                                    <option value="2001">2001</option>
                                    <option value="2000">2000</option>
                                    <option value="1999">1999</option>
                                    <option value="1998">1998</option>
                                    <option value="1997">1997</option>
                                    <option value="1996">1996</option>
                                    <option value="1995">1995</option>
                                    <option value="1994">1994</option>
                                    <option value="1993">1993</option>
                                    <option value="1992">1992</option>
                                    <option value="1991">1991</option>
                                    <option value="1990">1990</option>
                                    <option value="1989">1989</option>
                                    <option value="1988">1988</option>
                                    <option value="1987">1987</option>
                                    <option value="1986">1986</option>
                                    <option value="1985">1985</option>
                                    <option value="1984">1984</option>
                                    <option value="1983">1983</option>
                                    <option value="1982">1982</option>
                                    <option value="1981">1981</option>
                                    <option value="1980">1980</option>
                                    <option value="1979">1979</option>
                                    <option value="1978">1978</option>
                                    <option value="1977">1977</option>
                                    <option value="1976">1976</option>
                                    <option value="1975">1975</option>
                                    <option value="1974">1974</option>
                                    <option value="1973">1973</option>
                                    <option value="1972">1972</option>
                                    <option value="1971">1971</option>
                                    <option value="1970">1970</option>
                                    <option value="1969">1969</option>
                                    <option value="1968">1968</option>
                                    <option value="1967">1967</option>
                                    <option value="1966">1966</option>
                                    <option value="1965">1965</option>
                                    <option value="1964">1964</option>
                                    <option value="1963">1963</option>
                                    <option value="1962">1962</option>
                                    <option value="1961">1961</option>
                                    <option value="1960">1960</option>
                                    <option value="1959">1959</option>
                                    <option value="1958">1958</option>
                                    <option value="1957">1957</option>
                                    <option value="1956">1956</option>
                                    <option value="1955">1955</option>
                                    <option value="1954">1954</option>
                                    <option value="1953">1953</option>
                                    <option value="1952">1952</option>
                                    <option value="1951">1951</option>
                                    <option value="1950">1950</option>
                                    <option value="1949">1949</option>
                                    <option value="1948">1948</option>
                                    <option value="1947">1947</option>
                                    <option value="1946">1946</option>
                                    <option value="1945">1945</option>
                                    <option value="1944">1944</option>
                                    <option value="1943">1943</option>
                                    <option value="1942">1942</option>
                                    <option value="1941">1941</option>
                                    <option value="1940">1940</option>
                                    <option value="1939">1939</option>
                                    <option value="1938">1938</option>
                                    <option value="1937">1937</option>
                                    <option value="1936">1936</option>
                                    <option value="1935">1935</option>
                                    <option value="1934">1934</option>
                                    <option value="1933">1933</option>
                                    <option value="1932">1932</option>
                                    <option value="1931">1931</option>
                                    <option value="1930">1930</option>
                                    <option value="1929">1929</option>
                                    <option value="1928">1928</option>
                                    <option value="1927">1927</option>
                                    <option value="1926">1926</option>
                                    <option value="1925">1925</option>
                                    <option value="1924">1924</option>
                                    <option value="1923">1923</option>
                                    <option value="1922">1922</option>
                                    <option value="1921">1921</option>
                                    <option value="1920">1920</option>
                                    <option value="1919">1919</option>
                                    <option value="1918">1918</option>
                                    <option value="1917">1917</option>
                                    <option value="1916">1916</option>
                                    <option value="1915">1915</option>
                                    <option value="1914">1914</option>
                                    <option value="1913">1913</option>
                                    <option value="1912">1912</option>
                                    <option value="1911">1911</option>
                                    <option value="1910">1910</option>
                                    <option value="1909">1909</option>
                                    <option value="1908">1908</option>
                                    <option value="1907">1907</option>
                                    <option value="1906">1906</option>
                                    <option value="1905">1905</option>
                                    <option value="1904">1904</option>
                                    <option value="1903">1903</option>
                                    <option value="1902">1902</option>
                                    <option value="1901">1901</option>
                                    <option value="1900">1900</option>
                                    <option value="1899">1899</option>
                                    <option value="1898">1898</option>
                                    <option value="1897">1897</option>
                                    <option value="1896">1896</option>
                                    <option value="1895">1895</option>
                                    <option value="1894">1894</option>
                                    <option value="1893">1893</option>
                                    <option value="1892">1892</option>
                                    <option value="1891">1891</option>

                                </select>
                            </div>
                            <div className="form__input-container">
                                <label className="form__label">Email *</label>
                                <input
                                    className="form__input"
                                    type="email"
                                    placeholder="Please enter your email address"
                                    onChange={(e) => {
                                        this.setState({
                                            ...this.state,
                                            formData: {
                                                ...this.state.formData,
                                                payment: {
                                                    ...this.state.formData.payment,
                                                    email: e.target.value,
                                                }
                                            }
                                        });
                                    }}
                                />
                            </div>
                            <div className="form__input-container">
                                <label className="form__label">Mobile Number *</label>
                                <input
                                    className="form__input"
                                    type="text"
                                    pattern="\d*"
                                    required
                                    placeholder="Please enter your mobile number"
                                    onChange={(e) => {
                                        this.setState({
                                            ...this.state,
                                            formData: {
                                                ...this.state.formData,
                                                payment: {
                                                    ...this.state.formData.payment,
                                                    mobileNumber: e.target.value,
                                                }
                                            }
                                        });
                                    }}
                                />
                            </div>
                            <div className="form__input-container">
                                <label className="form__label">Card Details *</label>
                                <input
                                    className="form__input"
                                    type="text"
                                    pattern="\d*"
                                    required
                                    placeholder="Card number"
                                    onChange={(e) => {
                                        this.setState({
                                            ...this.state,
                                            formData: {
                                                ...this.state.formData,
                                                payment: {
                                                    ...this.state.formData.payment,
                                                    cardDetails: {
                                                        ...this.state.formData.payment.cardDetails,
                                                        number: e.target.value,
                                                    }
                                                }
                                            }
                                        });
                                    }}
                                />
                                <input
                                    className="form__input form__input--small"
                                    type="text"
                                    required
                                    pattern="\d*"
                                    placeholder="CVC"
                                    onChange={(e) => {
                                        this.setState({
                                            ...this.state,
                                            formData: {
                                                ...this.state.formData,
                                                payment: {
                                                    ...this.state.formData.payment,
                                                    cardDetails: {
                                                        ...this.state.formData.payment.cardDetails,
                                                        cvc: e.target.value,
                                                    }
                                                }
                                            }
                                        });
                                    }}
                                />
                            </div>
                            <div className="form__input-container">
                                <label className="form__label">Card Expiry *</label>
                                <select className="form__input form__input--small" required placeholder="Month" onChange={(e) => {
                                    this.setState({
                                        ...this.state,
                                        formData: {
                                            ...this.state.formData,
                                            payment: {
                                                ...this.state.formData.payment,
                                                cardDetails: {
                                                    ...this.state.formData.payment.cardDetails,
                                                    expiry: {
                                                        ...this.state.formData.payment.cardDetails.expiry,
                                                        month: e.target.value
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }}>
                                    <option selected disabled>Month</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <select className="form__input form__input--small" required placeholder="Year" onChange={(e) => {
                                    this.setState({
                                        ...this.state,
                                        formData: {
                                            ...this.state.formData,
                                            payment: {
                                                ...this.state.formData.payment,
                                                cardDetails: {
                                                    ...this.state.formData.payment.cardDetails,
                                                    expiry: {
                                                        ...this.state.formData.payment.cardDetails.expiry,
                                                        year: e.target.value
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }}>>
                                    <option selected disabled>Year</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                </select>
                            </div>

                            <div className="form__options-container">
                                <input className="form__submit" type="button" value="Back" onClick={this.stepDecrease} />
                                <input className="form__submit" type="submit" value="Confirm" />
                            </div>
                        </form>
                    </>
                    }

                    {this.state.formData.step === 9 &&
                        <div className="overview">
                            <div className="overview__item">
                                <span className="overview__item-title">Existing Patient:</span>
                                <span className="overview__item-value">{this.state.formData.isExistingPatient ? "Yes" : "No"}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Treatment Type:</span>
                                <span className="overview__item-value">{this.state.formData.category}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Treatment Location:</span>
                                <span className="overview__item-value">{this.state.formData.location}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Practitioner:</span>
                                <span className="overview__item-value">{this.state.formData.practitioner}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Time Slot:</span>
                                <span className="overview__item-value">{this.state.formData.timeSlot}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Appointment Date:</span>
                                <span className="overview__item-value">{this.state.formData.date}</span>
                            </div>
                            <hr />
                            <div className="overview__item">
                                <span className="overview__item-title">Name:</span>
                                <span className="overview__item-value">{this.state.formData.payment.firstName} {this.state.formData.payment.lastName}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Date of Birth:</span>
                                <span className="overview__item-value">{this.state.formData.payment.dob.day}/{this.state.formData.payment.dob.month}/{this.state.formData.payment.dob.year}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Email:</span>
                                <span className="overview__item-value">{this.state.formData.payment.email}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Mobile Number:</span>
                                <span className="overview__item-value">{this.state.formData.payment.mobileNumber}</span>
                            </div>
                            <hr />
                            <div className="overview__item">
                                <span className="overview__item-title">Card Number:</span>
                                <span className="overview__item-value">{this.state.formData.payment.cardDetails.number}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Card CVC:</span>
                                <span className="overview__item-value">{this.state.formData.payment.cardDetails.cvc}</span>
                            </div>
                            <div className="overview__item">
                                <span className="overview__item-title">Card Expiry:</span>
                                <span className="overview__item-value">{this.state.formData.payment.cardDetails.expiry.month}/{this.state.formData.payment.cardDetails.expiry.year}</span>
                            </div>
                            <hr />
                            <form className="form">
                                <div className="form__options-container">
                                    <input className="form__submit" type="button" value="Back" onClick={this.stepDecrease} />
                                    <input className="form__submit" type="button" value="Pay" />
                                </div>
                            </form>
                        </div>
                    }
                </div>
            </div>
        );
    }
}


BookAppointment.propTypes = {
    bookAppointment: PropTypes.func,
};
