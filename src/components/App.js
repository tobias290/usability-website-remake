import React from "react";
import "../css/app.css";
import BookingPage from "./BookingPage";

const App = () => (
    <div className="App">
        <BookingPage />
    </div>
);

export default App;
